//SELECTORS 
const todoInput = document.querySelector(".todo-input");
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const todoOption = document.querySelector(".select-todo");


 // EVENT LISTENERS
 // ('click', function)
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);
todoOption.addEventListener('click', selectTodo);
document.addEventListener('DOMContentLoaded', getTodo)

 // FUNCTIONS
 function addTodo(e){
	// prevent form from submitting
	e.preventDefault();
	// todo div
	const todoDiv = document.createElement("div");
	todoDiv.classList.add("todo");

	// creating list
	const newTodo = document.createElement("li");
	newTodo.innerText = todoInput.value;
	newTodo.classList.add("todo-item");
	todoDiv.appendChild(newTodo);

	// Adding todo in the LocalStrorage
	saveToLocal(todoInput.value);



	// creating completed list button
	const checkButton = document.createElement('button');
	checkButton.innerHTML = '<i class="fas fa-check"></i>';
	checkButton.classList.add("check-btn");
	todoDiv.appendChild(checkButton)


	// creating trash list button
	const trashButton = document.createElement('button');
	trashButton.innerHTML = '<i class="fas fa-trash"></i>';
	trashButton.classList.add("trash-btn");
	todoDiv.appendChild(trashButton)

	// append to the list
	todoList.appendChild(todoDiv)
	todoInput.value = "";

};

function deleteCheck(e){
	const item = e.target;
	// delete Item
	if( item.classList[0] === "trash-btn"){
		const todo = item.parentElement;

		// adding the animation before removing/deleting the item
		todo.classList.add('animation');
		removeLocal(todo);
		todo.addEventListener('transitionend', function(){
			todo.remove();
		});
	};

	// check mark
	if( item.classList[0] === "check-btn"){
		const todo = item.parentElement;
		todo.classList.toggle('completed');
		// to change the color of check mark of a list item
		item.classList.toggle('bgc-btn')
	};


};

function selectTodo(e){
	// we have access to .todo-list, so we access its children using the code below and check if the child has className of "completed"
	let select = document.getElementsByClassName('todo')
	for( let i = 0; i < select.length; i++){
		// this code checks the value of the radio and display the list item
		switch(e.target.value){
			case 'all':
				select[i].style.display = 'flex'
				break;
			case 'completed':
				if(select[i].classList.contains('completed')){
					select[i].style.display = 'flex'
				} else {
					select[i].style.display = 'none'
				}
				break;
			case 'unfinished':
				if(!select[i].classList.contains('completed')){
					select[i].style.display = 'flex'
				} else {
					select[i].style.display = 'none'
				}
				break;
		};
		// dispalyCompleted(select[i])
	};
};


function saveToLocal(todo){
	// checking if I have existing todos in localstroge
	let todos;
	if(localStorage.getItem('todos') === null){
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem('todos'));
	};
	todos.push(todo);
	localStorage.setItem('todos', JSON.stringify(todos));
};

// this function is responsible for displaying the data stored in localstorage 
function getTodo(){
	// checking if I have existing todos in localstroge
	let todos;
	if(localStorage.getItem('todos') === null){
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem('todos'));
	};

	// this code is to display the stored data in localstorage
	todos.forEach( function(todo){
		const todoDiv = document.createElement("div");
	todoDiv.classList.add("todo");

	// creating list
	const newTodo = document.createElement("li");
	newTodo.innerText = todo;
	newTodo.classList.add("todo-item");
	todoDiv.appendChild(newTodo);

	// completed list button
	const checkButton = document.createElement('button');
	checkButton.innerHTML = '<i class="fas fa-check"></i>';
	checkButton.classList.add("check-btn");
	todoDiv.appendChild(checkButton)


	// trash list button
	const trashButton = document.createElement('button');
	trashButton.innerHTML = '<i class="fas fa-trash"></i>';
	trashButton.classList.add("trash-btn");
	todoDiv.appendChild(trashButton)

	// append to the list
	todoList.appendChild(todoDiv)
	});
};


function removeLocal(todo){
	let todos;
	if(localStorage.getItem('todos') === null){
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem('todos'));
	};
	const todoLocalIndex = todo.children[0].innerText;
	todos.splice(todos.indexOf(todoLocalIndex), 1);
	localStorage.setItem("todos", JSON.stringify(todos));
};
